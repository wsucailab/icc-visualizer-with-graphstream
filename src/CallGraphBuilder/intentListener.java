package CallGraphBuilder;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Sabatu on 4/22/2017.
 */
public class intentListener {


   private static ServerSocket serverSocket;
    private static Socket clientSocket;
    private static InputStreamReader inputStreamReader;
    private static BufferedReader bufferedReader;
    private static String message = new String();

    public intentListener(){}

    public static void main(String[] args) {

        new Thread(new appServer()).start();
    }
        static class appServer implements Runnable {


            Process p;
            BufferedReader br;

            appServer() {}

            private ArrayList<IntentType> capturedIntent = new ArrayList<>();
            private int intentCounter = 0;
            private int messageRecvCount = 0;

            public void run() {

                try
                {
                    serverSocket = new ServerSocket(7575);  //Server socket
                } catch (IOException e)

                {
                    System.out.println("Could not listen on port: 7575");
                }

                System.out.println("Server started. Listening to the port 7575");

                //The type of graph renderer can be switched between dynCGMXGraph (for MXGraph) and callgraphVisualization (GraphStream) here

                JFrame mainFrame = new JFrame();

                callgraphVisualization callgraph = new callgraphVisualization(mainFrame);

                mainFrame.getContentPane().add(callgraph);
                Dimension DEFAULT_SIZE = new Dimension( 1500, 1000 );
                mainFrame.resize(DEFAULT_SIZE );
                mainFrame.setVisible(true);

                while (true)
                {

                    try {

                        clientSocket = serverSocket.accept();   //accept the client connection
                        inputStreamReader = new InputStreamReader(clientSocket.getInputStream());
                        bufferedReader = new BufferedReader(inputStreamReader); //get the client message
                        message = bufferedReader.readLine();

                        if(message != null)
                        {
                            if(messageRecvCount == 0)
                            {
                                String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
                                capturedIntent.add(new IntentType(timeStamp));
                                capturedIntent.get(intentCounter).setTimestamp(timeStamp);
                            }
                            if(message.contains("ΨΩ∇") && message.contains("Type")) {

                                if (message.contains("sent")) {

                                    capturedIntent.get(intentCounter).setSendOrRecv("Sent");
                                }

                                if(message.contains("Received"))
                                {
                                    capturedIntent.get(intentCounter).setSendOrRecv("Received");
                                }

                                messageRecvCount +=1;

                            } //<=== end of parsing Type input

                            if(message.contains("αβγ") && message.contains("Caller")) {

                                try {
                                    int beginIndex = 0;
                                    int endIndex = 0;
                                    int indexBeforeCaller = 0;

                                    beginIndex = message.indexOf("<");
                                    String completeCallStatement = message.substring(beginIndex);
                                    beginIndex = completeCallStatement.indexOf("<");
                                    endIndex = completeCallStatement.indexOf(":");
                                    String trimmedString = completeCallStatement.substring(beginIndex, endIndex);
                                    indexBeforeCaller = trimmedString.lastIndexOf(".");
                                    indexBeforeCaller += 1;
                                    String caller = trimmedString.substring(indexBeforeCaller, endIndex);

                                    capturedIntent.get(intentCounter).setCaller(caller);
                                    messageRecvCount += 1;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } //<< -- end of parsing caller input

                            if(message.contains("ηθι") && message.contains("Callsite"))
                            {
                                try {
                                    int beginIndex = 0;
                                    int endIndex = 0;

                                    beginIndex = message.indexOf("<");
                                    endIndex = message.indexOf(">");
                                    String callString = message.substring(beginIndex, endIndex);
                                    beginIndex = callString.indexOf(":");
                                    beginIndex += 1;
                                    endIndex = callString.indexOf("(");
                                    String callSite = callString.substring(beginIndex,endIndex);
                                    callSite += "()";

                                    capturedIntent.get(intentCounter).setCallsite(callSite);
                                    messageRecvCount +=1;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }





                            } //<< == end of callsite processing

                            if(message.contains("πρς") && message.contains("Line"))
                            {
                                try {
                                    int beginIndex = 0;

                                    beginIndex = message.indexOf(":");
                                    beginIndex += 1;

                                    String javaLineNumber = message.substring(beginIndex);
                                    capturedIntent.get(intentCounter).setlineNumber(javaLineNumber);
                                    messageRecvCount += 1;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            if(message.contains("δεζ") && message.contains("Component"))
                            {
                                try {
                                    int beginIndex = 0;

                                    beginIndex = message.indexOf(":") + 1;

                                    beginIndex += 1;

                                    String component = message.substring(beginIndex);
                                    capturedIntent.get(intentCounter).setcomponentOfClass(component);

                                    messageRecvCount +=1;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            } // <== end of component processing

                            if(message.contains("κμξ") && message.contains("Verbose"))
                            {

                                /*If user backs out of a menu UI choice that triggers an intent, portions of the intent code still get picked up by the instrumentation;
                                generating an imcomplete intent. if(message.length() <= 23) checks for this, and updates the ID to a value that will be checked
                                in the graph generation classfile; retaining the data but not adding it to part of the graph structure or any other metrics.)
                                 */
                                if(message.length() <= 26)
                                {

                                    capturedIntent.get(intentCounter).setID("User aborted intent");

                                    messageRecvCount += 1;
                                }

                                else {


                                    int beginIndex = 0;
                                    int endIndex = 0;

                                    String Action = new String();
                                    String packageName = new String();
                                    String DataString = new String();
                                    String DataURI = new String();
                                    String Scheme = new String();
                                    String Flags = new String();
                                    String Type = new String();
                                    String Extras = new String();
                                    String Component = new String();

                                    //============================ Capture Action ============================
                                    beginIndex = message.indexOf("Action:");
                                    beginIndex += 7;
                                    endIndex = message.indexOf("λ", beginIndex);

                                    Action = message.substring(beginIndex, endIndex);


                                    capturedIntent.get(intentCounter).setAction(Action);

                                    //============================ Capture packageName ============================

                                    beginIndex = message.indexOf("PackageName:");
                                    beginIndex += 12;
                                    endIndex = message.indexOf("λ", beginIndex);

                                    packageName = message.substring(beginIndex, endIndex);


                                    capturedIntent.get(intentCounter).setPackageName(packageName);

                                    //============================ Capture DataString ============================

                                    beginIndex = message.indexOf("DataString:");
                                    beginIndex += 11;
                                    endIndex = message.indexOf("λ", beginIndex);

                                    DataString = message.substring(beginIndex, endIndex);



                                    capturedIntent.get(intentCounter).setdataString(DataString);

                                    //============================ Capture DataURI ============================

                                    beginIndex = message.indexOf("DataURI:");
                                    beginIndex += 8;
                                    endIndex = message.indexOf("λ", beginIndex);

                                    DataURI = message.substring(beginIndex, endIndex);



                                    capturedIntent.get(intentCounter).setdataURI(DataURI);

                                    //============================ Capture Scheme ============================

                                    beginIndex = message.indexOf("Scheme:");
                                    beginIndex += 7;
                                    endIndex = message.indexOf("λ", beginIndex);

                                    Scheme = message.substring(beginIndex, endIndex);

                                    capturedIntent.get(intentCounter).setScheme(Scheme);

                                    //============================ Capture Flags ============================

                                    beginIndex = message.indexOf("Flags:");
                                    beginIndex += 6;
                                    endIndex = message.indexOf("λ", beginIndex);

                                    Flags = message.substring(beginIndex, endIndex);

                                    capturedIntent.get(intentCounter).setFlags(Flags);

                                    //============================ Capture Type ============================

                                    beginIndex = message.indexOf("Type:");
                                    beginIndex += 5;
                                    endIndex = message.indexOf("λ", beginIndex);

                                    Type = message.substring(beginIndex, endIndex);

                                    capturedIntent.get(intentCounter).setType(Type);

                                    //============================ Capture Extras ============================

                                    beginIndex = message.indexOf("Extras:");
                                    beginIndex += 7;
                                    endIndex = message.indexOf("λ", beginIndex);

                                    Extras = message.substring(beginIndex, endIndex);

                                    capturedIntent.get(intentCounter).setExtras(Extras);

                                    //============================ Capture Component ============================

                                    beginIndex = message.indexOf("Component:");
                                    beginIndex += 10;
                                    endIndex = message.indexOf("λ", beginIndex);

                                    Component = message.substring(beginIndex, endIndex);



                                    capturedIntent.get(intentCounter).setComponent(Component);

                                    messageRecvCount += 1;

                                }
                            }

                            if(messageRecvCount == 6)
                            {


                                if(capturedIntent.get(intentCounter).getID() == "User aborted intent")
                                {
                                       /*
                                         This is a temporary handling mechanism for a bug in the instrumenation code:
                                         If the user backs out of a UI selection that generates an intent, portions of the intent generation code still trip
                                         the intent data transmission to this program, creating a faulty intent where one actually never occurred. The erronous intent is tagged
                                         in intentListener with an ID of "User aborted intent", and this conditional ensures that the intent will not be added into the graph.

                                       */

                                    capturedIntent.get(intentCounter).setSendOrRecv("Neither");
                                }

                                if(capturedIntent.get(intentCounter).getSendOrRecv() == "Sent")
                                {
                                    callgraph.updateGraph(capturedIntent, intentCounter);



                                }

                                if(capturedIntent.get(intentCounter).getSendOrRecv() == "Received")
                                {
                                    callgraph.updateGraph(capturedIntent, intentCounter);



                                }

                                messageRecvCount = 0;

                                intentCounter += 1;
                            }




                        }

                        inputStreamReader.close();
                        clientSocket.close();


                    } catch (IOException ex) {
                        System.out.println("Problem in message reading");
                    }
                }

            }
        }
    }


