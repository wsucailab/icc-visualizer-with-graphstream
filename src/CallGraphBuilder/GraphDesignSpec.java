package CallGraphBuilder;

import java.awt.*;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Sabatu on 7/7/2017.
 */
public class GraphDesignSpec {

    //These are all specifically defined colors used by the static and dynamic graphs
    private Color rcvICCColor = new Color(255, 0, 127);
    private Color sendICCColor = new Color(128,128,128);
    private Color extMatchNodeColor = Color.blue;
    private Color ImpIntColor = new Color(153,255,255);
    private Color ExpIntColor = new Color(127,0,255);
    private Color ImpExtColor = new Color(255,0,0);
    private Color ExpExtColor = new Color(255,153,51);
    private Color ActivityColor = new Color(0,255,0);
    private Color ServicesColor = new Color(255,102,255);
    private Color BroadcastrcvrColor = new Color(255,255,102);
    private Color ContentprvdrColor = new Color(204,229,255);


    /*This ArrayList is used in ensuring that the utility function that picks random colors for the caller nodes
* does not make the mistake of picking a color that's already explicitly in use by other ICC-Vis analysis attributes.*/
    private ArrayList<Color> specColors = new ArrayList<>(Stream.of(rcvICCColor,sendICCColor,extMatchNodeColor,
            ActivityColor,ServicesColor,BroadcastrcvrColor,ContentprvdrColor,ImpIntColor,ExpIntColor,
            ImpExtColor,ExpExtColor).collect(Collectors.toList()));

    //These are the stored CSS style sheets for the various node/edge/sprite types used in the dynamic and static callgraphs

    private String nodeStyleSent = "fill-color:" + utilities.returnColorString(sendICCColor) +";"   +
            "size: 25px, 25px, 15px; shape: circle; stroke-mode: dots; " +
            "stroke-color: black; shadow-mode: gradient-radial; shadow-offset: 0; shadow-color: rgb(0,0,51); " +
            "text-background-mode: rounded-box; text-alignment: under;" +
            "text-color: black; text-offset: 0, -10; " +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.075; ";

    private String nodeStyleRecv = "shape: rounded-box; fill-color:" + utilities.returnColorString(rcvICCColor) +";"   +
            "size: 25px, 25px, 15px; stroke-mode: dots; " +
            "stroke-color: black; shadow-mode: gradient-radial;" +
            "text-background-mode: rounded-box; text-alignment: under;" +
            "text-color: black; text-offset: 0, -10;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.075; ";

    private String nodeStyleCaller = "shape: circle; fill-color: rgb(153, 0, 255);  " +
            "size: 15px, 15px, 1px; text-color: black; size-mode: fit; text-alignment: under;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.075;";

    private String ActivityCaller = "shape: rounded-box; fill-color: "+ utilities.returnColorString(ActivityColor) +
            "; size: 15px, 15px, 1px; text-color: black; size-mode: fit; text-alignment: under;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.075;";

    private String ServicesCaller = "shape: rounded-box; fill-color: "+ utilities.returnColorString(ServicesColor) +
            "; size: 15px, 15px, 1px; text-color: black; size-mode: fit; text-alignment: under;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.075;";

    private String BroadcastrcvrCaller = "shape: rounded-box; fill-color: "+ utilities.returnColorString(BroadcastrcvrColor) +
            "; size: 15px, 15px, 1px; text-color: black; size-mode: fit; text-alignment: under;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.075;";

    private String ContentprvdrCaller = "shape: rounded-box; fill-color: "+ utilities.returnColorString(ContentprvdrColor) +
            "; size: 15px, 15px, 1px; text-color: black; size-mode: fit; text-alignment: under;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.075;";

    private String nodeStyleExternal = "shape: diamond; fill-color:" + utilities.returnColorString(extMatchNodeColor) +";"   +
            "size: 25px, 25px, 15px; stroke-mode: dots; " +
            "stroke-color: black; shadow-mode: gradient-radial;" +
            "text-background-mode: rounded-box; text-alignment: under;" +
            "text-color: black; text-offset: 0, -10;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.075;";


    private String spriteStyle = "fill-color: rgb(204,204,255);  " +
            "size: 1px, 1px, 1px; shape: rounded-box; stroke-mode: dots; " +
            "stroke-color: black; shadow-mode: plain; " +
            "text-background-mode: rounded-box; text-alignment: under;" +
            "text-color: black; text-offset: 0, -2; " +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.075;";

    private String spriteStyleVisible = "fill-color: rgb(255,0,0);  " +
            "size: 10px, 10px, 10px; shape: rounded-box; stroke-mode: dots; " +
            "stroke-color: black; shadow-mode: plain; " +
            "text-background-mode: rounded-box; text-alignment: center;" +
            "text-color: black; text-offset: 0, -2; " +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.075;";


    private String edgeStyleTime = "arrow-shape: arrow; fill-color: rgb(0,0,51); text-color: black; " +
            " text-alignment: along; arrow-size: 10px,5px; text-background-color: white; text-padding: 3px, 3px; text-background-mode: plain;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.05; text-size: 7; text-offset: 20, 20;";


    private String edgeStyleFragment = "shape: cubic-curve; arrow-shape: circle; fill-color: rgb(0,128,255); text-color: black; " +
            " text-alignment: along; arrow-size: 10px,5px;text-background-color: white; text-padding: 3px, 3px; text-background-mode: rounded-box;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.035; text-size: 10;";


    private String edgeStyleImpInternal  = "shape: cubic-curve; arrow-shape: arrow; fill-color:" + utilities.returnColorString(ImpIntColor) +";  text-color: black; " +
            " text-alignment: along; arrow-size: 10px,5px; text-background-color: white; text-padding: 3px, 3px; text-background-mode: rounded-box;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.035; text-size: 10;";

    private String edgeStyleImpExternal  = "shape: cubic-curve; arrow-shape: arrow; fill-color:" + utilities.returnColorString(ImpExtColor) +";  text-color: black; " +
            " text-alignment: along; arrow-size: 10px,5px;text-background-color: white; text-padding: 3px, 3px; text-background-mode: rounded-box;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.035; text-size: 10;";

    private String edgeStyleExpInternal  = "shape: cubic-curve; arrow-shape: arrow; fill-color:" + utilities.returnColorString(ExpIntColor) +";  text-color: black; " +
            " text-alignment: along; arrow-size: 10px,5px;text-background-color: white; text-padding: 3px, 3px; text-background-mode: rounded-box;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.035; text-size: 10;";

    private String edgeStyleExpExternal  = "shape: cubic-curve; arrow-shape: arrow; fill-color:" + utilities.returnColorString(ExpExtColor) +";  text-color: black; " +
            " text-alignment: along; arrow-size: 10px,5px;text-background-color: white; text-padding: 3px, 3px; text-background-mode: rounded-box;" +
            "text-visibility-mode: zoom-range; text-visibility: 0, 0.035; text-size: 10;";


    private String dynamicGraphStyle = "graph { fill-mode: gradient-diagonal1; fill-color: white, rgb(192,192,192); padding: 200px, 200px, 0px; }";

    private String staticGraphStyle = "graph { fill-mode: gradient-diagonal1; fill-color: white, rgb(192,192,192); padding: 200px, 200px, 0px; }";

    //These are the font settings for the pop-up window that displays verbose intent information
    private static Font detailWindowFont = new Font("Arial", 12,10);


    public Color getRcvICCColor() {
        return rcvICCColor;
    }

    public Color getSendICCColor() {
        return sendICCColor;
    }

    public Color getExtMatchNodeColor() {
        return extMatchNodeColor;
    }

    public Color getImpIntColor() {
        return ImpIntColor;
    }

    public Color getExpIntColor() {
        return ExpIntColor;
    }

    public Color getImpExtColor() {
        return ImpExtColor;
    }

    public Color getExpExtColor() {
        return ExpExtColor;
    }

    public Color getActivityColor() {
        return ActivityColor;
    }

    public Color getServicesColor() {
        return ServicesColor;
    }

    public Color getBroadcastrcvrColor() {
        return BroadcastrcvrColor;
    }

    public Color getContentprvdrColor() {
        return ContentprvdrColor;
    }

    public String getNodeStyleSent() {
        return nodeStyleSent;
    }

    public String getNodeStyleRecv() {
        return nodeStyleRecv;
    }

    public String getNodeStyleCaller() {
        return nodeStyleCaller;
    }

    public String getActivityCaller() {
        return ActivityCaller;
    }

    public String getServicesCaller() {
        return ServicesCaller;
    }

    public String getBroadcastrcvrCaller() {
        return BroadcastrcvrCaller;
    }

    public String getContentprvdrCaller() {
        return ContentprvdrCaller;
    }

    public String getNodeStyleExternal() {
        return nodeStyleExternal;
    }

    public String getSpriteStyle() {
        return spriteStyle;
    }

    public String getSpriteStyleVisible() {
        return spriteStyleVisible;
    }

    public String getEdgeStyleTime() {
        return edgeStyleTime;
    }

    public String getEdgeStyleFragment() {
        return edgeStyleFragment;
    }

    public String getEdgeStyleImpInternal() {
        return edgeStyleImpInternal;
    }

    public String getEdgeStyleImpExternal() {
        return edgeStyleImpExternal;
    }

    public String getEdgeStyleExpInternal() {
        return edgeStyleExpInternal;
    }

    public String getEdgeStyleExpExternal() {
        return edgeStyleExpExternal;
    }

    public ArrayList<Color> getSpecColors() {
        return specColors;
    }

    public static Font getDetailWindowFont() {
        return detailWindowFont;
    }
    public String getDynamicGraphStyle() {
        return dynamicGraphStyle;
    }
    public String getStaticGraphStyle() {
        return staticGraphStyle;
    }
}
