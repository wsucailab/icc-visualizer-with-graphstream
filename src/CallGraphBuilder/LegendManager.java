package CallGraphBuilder;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Sabatu on 6/10/2017.
 */
public class LegendManager {

    private int InternalGridX = 0;
    private int InternalGridY = 0;
    private Font headerFont = new Font("SansSerif", Font.BOLD, 10);
    private JPanel legendContainer = new JPanel();
    private GridBagConstraints cs = new GridBagConstraints();

    public LegendManager()
    {

    }


    public LegendManager(JPanel GraphContainer, GridBagConstraints constr,int initialX,int initialY) {

        legendContainer.setLayout(new GridBagLayout());

        cs = constr;

        cs.gridx = initialX;
        cs.gridy = initialY;
        cs.weightx = 0.5;
        cs.weighty = 0.5;


        GraphContainer.add(legendContainer, cs);



    }

    protected void setManager(JPanel GraphContainer, GridBagConstraints constr,int initialX,int initialY)
    {

        legendContainer.setLayout(new GridBagLayout());

        cs = constr;

        cs.gridx = initialX;
        cs.gridy = initialY;
        cs.weightx = 0.5;
        cs.weighty = 0.5;


        GraphContainer.add(legendContainer, cs);


    }




    protected void addHeader(String[] headerFields)
    {
        for (String header : headerFields)
        {
            JTextField headerText = new JTextField();
            headerText.setText(header);
            headerText.setEditable(false);
            headerText.setFont(headerFont);
            headerText.setHorizontalAlignment(JTextField.CENTER);

            cs.gridx = InternalGridX;
            cs.gridy = InternalGridY;

            legendContainer.add(headerText, cs);

            InternalGridX += 1;
        }

        InternalGridX = 0;
        InternalGridY = 1;
    }

    protected ArrayList<JTextField> addTypeField(Color legendColor, String legendShape, String description)
    {

        ArrayList<JTextField> statsFields = new ArrayList<>();

        JPanel newVisualDescriptor = new JPanel() {

            protected void paintComponent(Graphics g) {

                g.setColor(legendColor);

                if(legendShape.equals("Rectangle"))
                {
                    g.fillRect(15, 15, 40, 40);
                }
                else if(legendShape.equals("Circle"))
                {
                    g.fillOval(15, 15, 40, 40);
                }
                else if(legendShape.equals("Line"))
                {
                    g.fillRect(0, 35, 80, 2);
                }
            }; //<=== End of paintComponent
        }; //<== End of JPanel newfield Constructor

        cs.fill = GridBagConstraints.BOTH;
        cs.weightx = 0.5;
        cs.weighty = 0.5;
        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;

        newVisualDescriptor.setBorder(BorderFactory.createLineBorder(new Color(184,207,229)));

        legendContainer.add(newVisualDescriptor, cs);


        InternalGridX += 1;

        JTextField newTextDescription = new JTextField();
        newTextDescription.setText(description);
        newTextDescription.setEditable(false);
        newTextDescription.setFont(headerFont);
        newTextDescription.setHorizontalAlignment(JTextField.CENTER);

        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;

        legendContainer.add(newTextDescription, cs);

        InternalGridX += 1;

        JTextField fieldCounter = new JTextField();
        fieldCounter.setText("0");
        fieldCounter.setEditable(false);
        fieldCounter.setFont(headerFont);
        fieldCounter.setHorizontalAlignment(JTextField.CENTER);

        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;

        legendContainer.add(fieldCounter, cs);


        InternalGridX += 1;

        JTextField percentCounter = new JTextField();
        percentCounter.setText("0 %");
        percentCounter.setEditable(false);
        percentCounter.setFont(headerFont);
        percentCounter.setHorizontalAlignment(JTextField.CENTER);

        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;

        legendContainer.add(percentCounter, cs);


        InternalGridX = 0;
        InternalGridY += 1;

        statsFields.add(fieldCounter);
        statsFields.add(percentCounter);

        return statsFields;

    }

    /*

    protected JTextField addCallerField(String caller)
    {

        JPanel newVisualDescriptor = new JPanel() {

            protected void paintComponent(Graphics g) {

                g.setColor(legendColor);

                    g.fillOval(15, 15, 40, 40);


            }; //<=== End of paintComponent
        }; //<== End of JPanel newfield Constructor

        cs.fill = GridBagConstraints.BOTH;
        cs.weightx = 0.5;
        cs.weighty = 0.5;
        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;
        cs.anchor = LINE_END;

        newVisualDescriptor.setBorder(BorderFactory.createLineBorder(new Color(184,207,229)));

        legendContainer.add(newVisualDescriptor, cs);

        InternalGridX += 1;






        JTextField callerName = new JTextField();
        callerName.setText(caller);
        callerName.setEditable(false);
        callerName.setFont(headerFont);
        callerName.setHorizontalAlignment(JTextField.CENTER);

        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;
        cs.anchor = LINE_START;

        legendContainer.add(callerName, cs);

        InternalGridX += 1;

        JTextField occurrences = new JTextField();
        occurrences.setText("0");
        occurrences.setEditable(false);
        occurrences.setFont(headerFont);
        occurrences.setHorizontalAlignment(JTextField.CENTER);

        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;
        cs.anchor = LINE_END;

        legendContainer.add(occurrences, cs);

        InternalGridX += 1;

        JCheckBox showInStaticGraph = new JCheckBox();
        // showInStaticGraph.



        return occurrences;
    }

*/




    public void repaintLegend()
    {
        legendContainer.repaint();
    }

}
