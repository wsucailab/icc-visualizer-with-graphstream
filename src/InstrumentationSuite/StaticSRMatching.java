package InstrumentationSuite;

import soot.jimple.AssignStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.Stmt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sabatu on 6/10/2017.
 */
public class StaticSRMatching {


        final static String[] __IntentSendingAPIs = {
                "startActivity",
                "startActivities",
                "startActivityForResult",
                "startActivityFromChild",
                "startActivityFromFragment",
                "startActivityIfNeeded",
                "startNextMatchingActivity",
                "sendBroadcast",
                "sendBroadcastAsUser",
                "sendOrderedBroadcast",
                "sendOrderedBroadcastAsUser",
                "sendStickyBroadcast",
                "sendStickyBroadcastAsUser",
                "sendStickyOrderedBroadcast",
                "sendStickyOrderedBroadcastAsUser",
                "removeStickyBroadcast",
                "removeStickyBroadcastAsUser",
                "bindService",
                "startService",
                "stopService",
                "startIntentSender",
                "startIntentSenderForResult",
                "onCreate",
                "startIntentSenderFromChild"
        };

        final static List<String> g__IntentSendingAPIs = new ArrayList<String>(Arrays.asList(__IntentSendingAPIs));

        public static boolean is_IntentSendingAPI(Stmt u) {
            if (!u.containsInvokeExpr()) {
                return false;
            }
            InvokeExpr inv = u.getInvokeExpr();
            // simple and naive decision based on textual matching
            return g__IntentSendingAPIs.contains(inv.getMethod().getName());
        }

        //////////////////////////////////////////
        final static String[] __IntentReceivingAPIs = {
                "getIntent",
                "getParentActivityIntent",
        };

        final static List<String> g__IntentReceivingAPIs = new ArrayList<String> (Arrays.asList(__IntentReceivingAPIs));

        public static boolean is_IntentReceivingAPI(Stmt u) {
            if (!(u instanceof AssignStmt)) {
                return false;
            }
            if (!u.containsInvokeExpr()) {
                return false;
            }
            InvokeExpr inv = u.getInvokeExpr();
            // simple and naive decision based on textual matching
            return g__IntentReceivingAPIs.contains(inv.getMethod().getName());
        }

        public static boolean is_IntentReceivingAPI(String cs) {
            for (String s : g__IntentReceivingAPIs) {
                if (cs.contains(s)) return true;
            }
            return false;
        }

        public static boolean is_IntentSendingAPI(String cs) {
            for (String s : g__IntentSendingAPIs) {
                if (cs.contains(s)) return true;
            }
            return false;
        }
}
